import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";
import 'firebase/firestore';

import { JSONLoad } from "../util/jsonLoader";
import { ModuleInfo } from "../user/moduleInfo";
import EventEmitter from "eventemitter3";

const StringFormat = firebase.storage.StringFormat;

// necessary for firebaseui login.
window.firebase = firebase;

/**
 * @const {string} USER_SAVES - storage path to user saves.
 */
const USER_SAVES = 'usersaves';
const USER_MODULES = 'modules';
const CORE_MODULES = 'core';

const Config = {
	apiKey: "AIzaSyDa2Qj2VQvTzhG0MwzxS-IhQy9LYpCgrRM",
	authDomain: "theory-of-magic-49589.firebaseapp.com",
	databaseURL: "https://theory-of-magic-49589.firebaseio.com",
	projectId: "theory-of-magic-49589",
	storageBucket: "theory-of-magic-49589.appspot.com",
	messagingSenderId: "347528879664",
	appId: "1:347528879664:web:009d25dcaee9d0e0e317f5"
 };


/**
 * Remote management using Google Firebase.
 */
export const FBRemote = {

	emitter:new EventEmitter(),

	get userid() {
		return this.auth.currentUser.uid;
	},
	get loggedIn(){
		return this.auth.currentUser != null;
	},

	on( event, listener, context ){
		this.emitter.on(event,listener, context );
	},

	removeListener(event, listener, context  ) {
		this.emitter.removeListener(event, listener, context );
	},

	init(){

		firebase.initializeApp( Config );

		this.auth = firebase.auth();
		this.auth.onAuthStateChanged( user=>{

			if ( user ) {
				this.emitter.emit('login', user.uid );
			} else {
				this.emitter.emit( 'logout' );

			}

		});

		this.db = firebase.firestore();
		this.modulesDB = this.db.collection( USER_MODULES );

		this.storage = firebase.storage();

	},

	logout(){
		return this.auth.signOut();
	},

	/**
	 *
	 * @param {UserModule} module
	 * @returns {Promise.<UploadTask>}
	 */
	saveModule( module ) {

		if ( !module.moduleData ) return false;

		let json = JSON.stringify(module.moduleData );

		this.db.collection( USER_MODULES ).doc(module.name).get().then(
			snap=>{

				if ( snap.exists && snap.uid !== this.userid ) {
					throw new Error('Module name taken.');
				}
				return snap.ref;
			}
		).then(
			(ref)=>{
				ref.set(
					{
						name:module.name,
						desc:module.desc,
						uid:this.userid
					}
				)
			}
		).then( docRef=>{

			module.id = docRef.id;

			var ref = firebase.storage().ref( this.userPath( this.userid, module.name ) );

			// returns UploadTask
			return ref.putString( json, StringFormat.RAW );


		});


	},

	/**
	 *
	 * @param {ModuleInfo} module
	 * @returns {Promise.<>}
	 */
	removeModule( module ) {

		var ref = this.moduleRef(this.userid, module.name );
		return ref.delete();

	},

	userModules(uid) {

		if (!uid ) uid = this.userid;

		this.findModules({uid:uid}).then(

			snap=>{

				var docs = snap.docs;
				var a = [];
				for( let i = 0; i < docs.length; i++ ) {

					a.push( new ModuleInfo( docs[i].data ) );

				}

				return a;

			}

		);

	},

	/**
	 * Return a module query matching the params.
	 * @param {object} match
	 * @returns {Promise.<QuerySnapshot>}
	 */
	findModules( match ) {

		let query = this.modulesDB;
		for( let p in match ) {
			query = query.where( p, '==', match[p] );
		}

		return query.get();

	},

	/**
	 * Reads modules in storage directory.
	 * @param {string} uid - user whose modules to list.
	 * @returns {Promise<ModuleInfo>}
	 */
	readModules( uid ){

		if (!uid ) uid = this.userid;

		var ref = this.storage().ref( this.userPath(this.userid ) );
		return ref.listAll().then(
			list=>{

				return list.items.map( ref=>new ModuleInfo(ref.name, uid ) );

			},
			err=>{
				return null;
			}

		);

	},

	/**
	 *
	 * @param {*} modId
	 * @returns {Promise<object>}
	 */
	loadModule( modId ){

		if ( !modId ) return null;

		var ref = this.moduleRef(this.userid, modId );
		return ref.getDownloadURL().then( url=>JSONLoad(url, false), err=>{
			console.warn(err);
			return null;
		});

	},

	/**
	 *
	 * @param {string} save
	 * @param {string} [modId='default']
	 * @returns {Promise<>}
	 */
	saveModule( save, modId ){

		var ref = this.moduleRef(this.userid, modId );
		if (!ref) console.warn('no data store: ' + store);
		console.log('UPLOADING FILE DATA');

		return ref.putString( save, StringFormat.RAW );

	},

	/**
	 * Get module reference.
	 * @param {*} uid
	 * @param {*} modId
	 */
	moduleRef( uid, modName ){
		return this.storage().ref( USER_MODULES + '/' + uid + '/' + modName );
	},

	userPath( uid ) {
		return USER_MODULES + '/' + uid
	},

	modulePath( modName ) {
		return USER_MODULES + '/' + modName;
	}

}

FBRemote.init();