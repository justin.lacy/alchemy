import { ModuleInfo } from "./moduleInfo";

export class UserModule extends ModuleInfo {


	/**
	 * @property {object} moduleData - actual data of the module file.
	 */
	get moduleData(){
		return this._moduleData;
	}
	set moduleData(v){
		this._moduleData =v;
	}

	get text(){ return this._text; }
	set text(v) { this._text = v;}

	constructor(){

		super();

	}

}