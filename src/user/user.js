export class User {

	/**
	 * firebase userid
	 */
	get userid(){return this._userid;}
	set userid(v){this._userid=v}

	get username(){return this._username;}
	set username(v){this._username=v}

	constructor(){
	}

}