/**
 * Information about a User module file.
 */
export class ModuleInfo {

	/**
	 * @property {string} id - internal database id of module.
	 */
	get id(){return this._id;}
	set id(v){ this._id = v;}

	/**
	 * @property {string} moduleId - unique string id of module.
	 */
	get moduleId() {return this._modId;}
	set moduleId(v) { this._modId=v}

	/**
	 * @property {string} name - human readable module name.
	 */
	get name(){ return this._name }
	set name(v){ this._name =v; }

	/**
	 * @property {string} desc - human readable module description.
	 */
	get desc(){return this._desc;}
	set desc(v){ this._desc = v;}

	/**
	 * @property {string} uid - id of module creator.
	 */
	get uid(){return this._uid;}
	set uid(v){ this._uid = v;}


	get creator(){return this._creator; }
	set creator(v){
		this._creator=v;
	}

	constructor( modId, uid ){

		this.id = modId;
		this.uid = uid;

	}

}