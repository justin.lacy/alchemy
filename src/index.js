import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from 'ui/main.vue';
import Confirm from 'ui/components/confirm.vue';

import {Profile} from 'user/profile';

Vue.mixin({

	components:{ confirm:Confirm },
	methods: {

		// get id for html element.
		elmId(name) { return name + this._uid; }

	}

});

const routes = [
	{ path:'', component:null},
	{ path:'', component:null},
];

const router = new VueRouter({
	routes
});

Vue.use(VueRouter);

const vm = new Vue({
	el: '#vueRoot',
	router,
	components:{
		Main
	},
	data(){
		return {
			// hacky re-render force. used to rerender on game reload.
			renderKey:1
		}
	},
	created(){

		this.lastUpload = 0;

		this.saveLink = null;

		this.loadProfile();

	},
	methods:{

		logout(){

			this.remote.logout();
			Profile.loggedIn=false;

		},

		loadProfile(){
		},

		/**
		 * Call on settings loaded.
		 * @param {*} vars
		 */
		onSettings(vars){

			if (!vars) return;

			this.onSetting( 'darkMode', vars.darkMode );
			this.onSetting( 'compactMode', vars.compactMode );
		},

		onSetting( setting, v ) {

			if ( setting === 'darkMode') {

				if ( v ) document.body.classList.add( 'darkmode');
				else document.body.classList.remove( 'darkmode');

			} else if ( setting === 'compactMode' ) {

				if ( v ) document.body.classList.add( 'compact');
				else document.body.classList.remove( 'compact');

			}

		},

		saveFile(e ){

			// event shouldnt be null but sometimes is.
			if (!e )return;
			try {

				if ( this.saveLink ) URL.revokeObjectURL( this.saveLink );

				let state = this.game.state;
				this.saveLink = this.makeLink( state, e.target, state.player.name );

			} catch(ex) {
				console.error( ex.message + '\n' + ex.stack );
			}

		},

		/**
		 * Create URL link for data.
		 * @param {object} data
		 * @param {HTMLElement} targ - link target.
		 * @returns {DOMString}
		 */
		makeLink( data, targ, saveName='arcanum' ) {

			let json = JSON.stringify(data);
			let file = new File( [json], saveName + '.json', {type:"text/json;charset=utf-8"} );

			targ.title = file.name;
			return targ.href = URL.createObjectURL( file );

		},

		loadFile(files) {

			const file = files[0];
			if ( !file) return;

			const reader = new FileReader();
			reader.onload = (e)=>{

				try {

					let data = JSON.parse( e.target.result );
				} catch(err){
					console.error(  err.message + '\n' + err.stack );
				}

			}
			reader.readAsText( file );

		},

	},
	render( createElm ) {
		return createElm( Main, { key:this.renderKey } );
	}

});